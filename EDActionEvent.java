import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.FileDialog;
import java.io.File;
import java.io.FileWriter;
import java.awt.Label;
import java.awt.Button;

public class EDActionEvent implements ActionListener {
    Encryption encryptionObj;
    Decryption decryptionObj;
    EDFrame frame;
    FileDialog fileDialog;
    boolean isDecryption;

    EDActionEvent(Encryption encryptionObj, Decryption decryptionObj) {
        this.encryptionObj = encryptionObj;
        this.decryptionObj = decryptionObj;
        if (this.encryptionObj == null) {
            this.isDecryption = true;
            this.frame = this.decryptionObj.frame;
        } else {
            this.isDecryption = false;
            this.frame = this.encryptionObj.frame;
        }
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        if (!this.isDecryption) {
            if (ae.getSource() == this.encryptionObj.getOpenFileButton()) {
                this.handleOpen("Select A File To Encrypt");
            } else if (ae.getSource() == this.encryptionObj.getSaveFileButton()) {
                this.handleSave("Select A Location To Encrypt");
            } else if (ae.getSource() == this.encryptionObj.getSubmitButton()) {
                this.handleSubmit();
            }
        } else {
            if (ae.getSource() == this.decryptionObj.getOpenFileButton()) {
                this.handleOpen("Select A File To Decrypt");
            } else if (ae.getSource() == this.decryptionObj.getSaveFileButton()) {
                this.handleSave("Select A Location To Decrypt");
            } else if (ae.getSource() == this.decryptionObj.getSubmitButton()) {
                this.handleSubmit();
            }
        }
    }

    private void handleOpen(String title) {
        this.fileDialog = new FileDialog(this.frame, title, FileDialog.LOAD);
        this.fileDialog.setVisible(true);

        if (this.fileDialog.getFile() != null) {
            if (!this.isDecryption) {

                this.encryptionObj.getOpenFileLabel().setText(fileDialog.getFile());
                this.encryptionObj.setOpenFileName(fileDialog.getFile());
                this.encryptionObj.setOpenFileDirectory(fileDialog.getDirectory());

                this.encryptionObj.setFileText(EDFileReader.getTextFromFile(
                        new File(this.encryptionObj.getOpenFileDirectory(), this.encryptionObj.getOpenFileName())));
            } else {
                this.decryptionObj.getOpenFileLabel().setText(fileDialog.getFile());
                this.decryptionObj.setOpenFileName(fileDialog.getFile());
                this.decryptionObj.setOpenFileDirectory(fileDialog.getDirectory());

                this.decryptionObj.setFileText(EDFileReader.getTextFromFile(
                        new File(this.decryptionObj.getOpenFileDirectory(), this.decryptionObj.getOpenFileName())));
            }
        }
    }

    private void handleSave(String title) {
        this.fileDialog = new FileDialog(this.frame, title, FileDialog.SAVE);
        this.fileDialog.setVisible(true);

        if (this.fileDialog.getFile() != null) {
            if (!this.isDecryption) {
                this.encryptionObj.getSaveFileLabel().setText(fileDialog.getFile());
                this.encryptionObj.setSaveFileName(fileDialog.getFile());
                this.encryptionObj.setSaveFileDirectory(fileDialog.getDirectory());
            } else {
                this.decryptionObj.getSaveFileLabel().setText(fileDialog.getFile());
                this.decryptionObj.setSaveFileName(fileDialog.getFile());
                this.decryptionObj.setSaveFileDirectory(fileDialog.getDirectory());
            }
        }
    }

    private void handleSubmit() {
        if (!this.isDecryption) {
            if (this.encryptionObj.getOpenFileName() == null
                    || this.encryptionObj.getOpenFileDirectory() == null
                    || this.encryptionObj.getSaveFileName() == null
                    || this.encryptionObj.getSaveFileDirectory() == null) {
                this.makeMissingFileErrorDialog(1);
                return;
            }
            String encryptedText = this.encryptionObj.getEncryptedText();

            File newFile = new File(this.encryptionObj.getSaveFileDirectory(), this.encryptionObj.getSaveFileName());
            try {
                newFile.createNewFile();
                EDFileWriter.setTextInFile(newFile, encryptedText);
            } catch (Exception e) {
                System.out.println("Some Error Occurred");
            }
            this.makeSuccessfulDialog(1);
        } else {
            if (this.decryptionObj.getOpenFileName() == null
                    || this.decryptionObj.getOpenFileDirectory() == null
                    || this.decryptionObj.getSaveFileName() == null
                    || this.decryptionObj.getSaveFileDirectory() == null) {
                this.makeMissingFileErrorDialog(2);
                return;
            }
            String decryptedText = this.decryptionObj.getDecryptedText();

            File newFile = new File(this.decryptionObj.getSaveFileDirectory(), this.decryptionObj.getSaveFileName());

            try {
                newFile.createNewFile();
                EDFileWriter.setTextInFile(newFile, decryptedText);
            } catch (Exception e) {
                System.out.println("Some Error Occurred");
            }
            this.makeSuccessfulDialog(2);
        }
    }

    private void makeMissingFileErrorDialog(int type) {
        EDDialog errorDialogBox = new EDDialog(this.frame, "Files Not Selected", true);
        Button ok = new Button("Ok");
        Button cancel = new Button("Cancel");
        Label errorLabel;
        if (type == 1) {
            errorLabel = new Label("Please Select All The Files For Encryption");
        } else {
            errorLabel = new Label("Please Select All The Files For Decryption");
        }

        ok.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                errorDialogBox.dispose();
            }
        });

        cancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                errorDialogBox.dispose();
            }
        });

        errorDialogBox.add(errorLabel);
        errorDialogBox.add(ok);
        errorDialogBox.add(cancel);

        errorDialogBox.setVisible(true);
    }

    private void makeSuccessfulDialog(int type) {
        EDDialog successDialogBox = new EDDialog(this.frame, "Successful Conversion", true);
        Button ok = new Button("Ok");
        Label successLabel;
        if (type == 1) {
            successLabel = new Label("Encryption Successful");
        } else {
            successLabel = new Label("Descryption Successful");
        }

        ok.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                successDialogBox.dispose();
            }
        });

        successDialogBox.add(successLabel);
        successDialogBox.add(ok);
        this.resetAllFields(type);
        successDialogBox.setVisible(true);
    }

    private void resetAllFields(int type) {
        if (type == 1) {
            this.encryptionObj.resetAllFields();
        } else {
            this.decryptionObj.resetAllFields();
        }
    }
}
