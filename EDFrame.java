import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Panel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Insets;
import java.awt.Dimension;

public class EDFrame extends Frame {
    Panel encryptionPanel;
    Panel decryptionPanel;

    EDFrame(String title) {
        super(title);
        setSize(610, 490);
        setLayout(new GridLayout(1, 2, 10, 10));
        setMinimumSize(new Dimension(590, 470));
        setBackground(Color.BLACK);

        setLocationRelativeTo(null);
        addWindowListener(new EDWindowClosingAdapter(this));

        this.encryptionPanel = new Panel(new BorderLayout(100, 100));
        this.decryptionPanel = new Panel(new BorderLayout(100, 100));

        this.encryptionPanel.setBackground(Color.GRAY);
        this.decryptionPanel.setBackground(Color.GRAY);

        this.add(this.encryptionPanel);
        this.add(this.decryptionPanel);

        new Encryption(this);
        new Decryption(this);
        setVisible(true);
    }

    public Insets getInsets() {
        return new Insets(50, 50, 50, 50);
    }
}
