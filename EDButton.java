import java.awt.Button;

public class EDButton extends Button {
    Encryption encryptionObj;
    Decryption decryptionObj;

    EDButton(Encryption encryptionObj) {
        super();
        this.encryptionObj = encryptionObj;
        addActionListener(new EDActionEvent(this.encryptionObj, this.decryptionObj));
    }

    EDButton(Encryption encryptionObj, String title) {
        super(title);
        this.encryptionObj = encryptionObj;
        addActionListener(new EDActionEvent(this.encryptionObj, this.decryptionObj));
    }

    EDButton(Decryption decryptionObj) {
        super();
        this.decryptionObj = decryptionObj;
        addActionListener(new EDActionEvent(this.encryptionObj, this.decryptionObj));
    }

    EDButton(Decryption decryptionObj, String title) {
        super(title);
        this.decryptionObj = decryptionObj;
        addActionListener(new EDActionEvent(this.encryptionObj, this.decryptionObj));
    }

    {
        setFont(EDFont.getFont());
    }
}
