import java.awt.BorderLayout;
import java.awt.Panel;

public class Decryption {
    EDFrame frame;
    private Panel panel;
    private EDLabel openFileLabel;
    private EDLabel saveFileLabel;
    private EDLabel typeLabel;
    private EDButton openFileButton;
    private EDButton saveFileButton;
    private EDButton submitButton;
    private EDChoice dropdown;

    private String openFileDirectory;
    private String openFileName;
    private String saveFileDirectory;
    private String saveFileName;

    private String fileText;
    final int KEY = 25;

    Decryption(EDFrame frame) {
        this.frame = frame;
        this.panel = this.frame.decryptionPanel;
        this.openFileLabel = new EDLabel("Select File", EDLabel.CENTER);
        this.saveFileLabel = new EDLabel("Save File", EDLabel.CENTER);
        this.typeLabel = new EDLabel("Decryption Type", EDLabel.CENTER);
        this.openFileButton = new EDButton(this, "Choose");
        this.saveFileButton = new EDButton(this, "Save");
        this.submitButton = new EDButton(this, "Submit");
        this.dropdown = new EDChoice();

        Panel openFilePanel = new Panel();
        openFilePanel.add(this.openFileLabel);
        openFilePanel.add(this.openFileButton);

        Panel saveFilePanel = new Panel();
        saveFilePanel.add(this.saveFileLabel);
        saveFilePanel.add(this.saveFileButton);

        Panel dropdownPanel = new Panel();
        dropdownPanel.add(this.dropdown);

        Panel submitPanel = new Panel();
        submitPanel.add(this.submitButton);

        Panel centerPanel = new Panel(new BorderLayout());
        centerPanel.add(typeLabel, BorderLayout.NORTH);
        centerPanel.add(dropdownPanel, BorderLayout.CENTER);
        centerPanel.add(saveFilePanel, BorderLayout.SOUTH);

        this.panel.add(openFilePanel, BorderLayout.NORTH);
        this.panel.add(centerPanel, BorderLayout.CENTER);
        this.panel.add(submitPanel, BorderLayout.SOUTH);
    }

    public EDButton getOpenFileButton() {
        return this.openFileButton;
    }

    public EDButton getSaveFileButton() {
        return this.saveFileButton;
    }

    public EDButton getSubmitButton() {
        return this.submitButton;
    }

    public EDLabel getOpenFileLabel() {
        return this.openFileLabel;
    }

    public EDLabel getSaveFileLabel() {
        return this.saveFileLabel;
    }

    public EDChoice getEDDropdown() {
        return this.dropdown;
    }

    public String getOpenFileName() {
        return this.openFileName;
    }

    public String getOpenFileDirectory() {
        return this.openFileDirectory;
    }

    public void setOpenFileName(String fileName) {
        this.openFileName = fileName;
    }

    public void setOpenFileDirectory(String fileDirectory) {
        this.openFileDirectory = fileDirectory;
    }

    public String getSaveFileName() {
        return this.saveFileName;
    }

    public String getSaveFileDirectory() {
        return this.saveFileDirectory;
    }

    public void setSaveFileName(String fileName) {
        this.saveFileName = fileName;
    }

    public void setSaveFileDirectory(String fileDirectory) {
        this.saveFileDirectory = fileDirectory;
    }

    public void setFileText(String fileText) {
        this.fileText = fileText;
    }

    public String getFileText() {
        return this.fileText;
    }

    public String getDecryptedText() {
        if (this.dropdown.getSelectedIndex() == this.dropdown.CEASER_CIPHER_PLAIN) {
            return this.performPlain();
        } else {
            return this.performAlpha();
        }
    }

    private String performPlain() {
        StringBuffer data = new StringBuffer("");
        for (int i = 0; i < this.fileText.length(); ++i) {
            char ch = this.fileText.charAt(i);
            if (this.isAlphabet(ch)) {
                int key = this.KEY % 26;
                int charAscii = (int) ch;
                int newCharAscii = charAscii - key;
                if (this.isUpperCase(ch)) {
                    if (newCharAscii < 65) {
                        newCharAscii += 26;
                    }
                }

                if (this.isLowerCase(ch)) {
                    if (newCharAscii < 97) {
                        newCharAscii += 26;
                    }
                }
                data.append((char) newCharAscii);
            } else if (this.isNumeric(ch)) {
                int key = this.KEY % 10;
                int charAscii = (int) ch;
                int newCharAscii = charAscii - key;
                if (newCharAscii < 48) {
                    newCharAscii += 10;
                }
                data.append((char) newCharAscii);
            } else {
                data.append(ch);
            }
        }

        return data.toString();
    }

    private String performAlpha() {
        StringBuffer data = new StringBuffer("");
        for (int i = 0; i < this.fileText.length(); ++i) {
            char ch = this.fileText.charAt(i);
            int key = this.KEY % 26;
            int charAscii = (int) ch;
            int newCharAscii = charAscii - key;
            if (newCharAscii < 97) {
                newCharAscii += 26;
            }
            data.append((char) newCharAscii);
        }
        return data.toString();
    }

    private boolean isAlphabet(char ch) {
        if ((ch >= 'A' && ch <= 'Z') || (ch >= 'a' && ch <= 'z')) {
            return true;
        }
        return false;
    }

    private boolean isNumeric(char ch) {
        if ((ch >= '0' && ch <= '9')) {
            return true;
        }
        return false;
    }

    private boolean isUpperCase(char ch) {
        if ((ch >= 'A' && ch <= 'Z')) {
            return true;
        }
        return false;
    }

    private boolean isLowerCase(char ch) {
        if ((ch >= 'a' && ch <= 'z')) {
            return true;
        }
        return false;
    }

    public void resetAllFields() {
        this.openFileLabel.setText("Select File");
        this.saveFileLabel.setText("Save File");
        this.openFileDirectory = null;
        this.saveFileDirectory = null;
        this.openFileName = null;
        this.saveFileName = null;
        this.fileText = null;
    }
}
