import java.awt.Dialog;
import java.awt.event.WindowEvent;
import java.awt.event.WindowAdapter;
import java.awt.FlowLayout;

public class EDDialog extends Dialog {
    EDDialog(EDFrame parent, String title) {
        super(parent, title);
    }

    EDDialog(EDFrame parent, String title, boolean mode) {
        super(parent, title, mode);
    }

    {
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent we) {
                dispose();
            }
        });

        setSize(220, 220);
        setLocationRelativeTo(null);
        setLayout(new FlowLayout());
    }

}
