import java.awt.Choice;

public class EDChoice extends Choice {
    final int CEASER_CIPHER_PLAIN = 0;
    final int CEASER_CIPHER_ALPHA = 1;

    EDChoice() {
        add("Ceaser Cipher Plain");
        add("Ceaser Cipher Alphabetic");
    }

    {
        setFont(EDFont.getFont());
    }
}
