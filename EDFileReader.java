import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.awt.event.KeyEvent;

public class EDFileReader {
    public static String getTextFromFile(File file) {

        StringBuffer completeData = new StringBuffer("");
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String tempData = "";
            while ((tempData = br.readLine()) != null) {
                completeData.append(tempData);
                completeData.append((char) KeyEvent.VK_ENTER);
            }
        } catch (Exception e) {
            System.out.println("Some Error Occured ");
        }
        return completeData.toString();
    }
}