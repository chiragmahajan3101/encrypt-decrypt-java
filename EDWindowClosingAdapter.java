import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.Label;
import java.awt.Button;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class EDWindowClosingAdapter extends WindowAdapter {
    private EDFrame EDframe;

    EDWindowClosingAdapter(EDFrame EDframe) {
        this.EDframe = EDframe;
    }

    public void windowClosing(WindowEvent we) {
        EDDialog dialogBox = new EDDialog(EDframe, "Encryption Decryption", true);

        Label closeLabel = new Label("Are You Sure You Want To Exit");
        Button yes = new Button("Yes");
        Button no = new Button("No");
        Button cancel = new Button("Cancel");

        yes.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                dialogBox.dispose();
                EDframe.dispose();
            }
        });

        no.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                dialogBox.dispose();
            }
        });

        cancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                dialogBox.dispose();
            }
        });

        dialogBox.add(closeLabel);
        dialogBox.add(yes);
        dialogBox.add(no);
        dialogBox.add(cancel);

        dialogBox.setVisible(true);
    }
}